package ru.t1.aksenova.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.aksenova.tm.entity.model.Project;
import ru.t1.aksenova.tm.enumerated.Status;
import ru.t1.aksenova.tm.repository.model.ProjectRepository;

import java.util.Collection;
import java.util.List;

public interface IProjectService {
    @NotNull ProjectRepository getRepository();

    @NotNull
    @Transactional
    Project add(@NotNull Project project);

    @Transactional
    void update(@NotNull Project project);

    @Transactional
    void remove(@NotNull Project project);

    @NotNull Project create(@Nullable Project project);

    @NotNull Project create(
            @Nullable String name,
            @Nullable String description
    );

    @NotNull
    @Transactional
    Collection<Project> set(@NotNull Collection<Project> projects);

    @NotNull List<Project> findAll();

    @Nullable Project findOneById(
            @Nullable String id
    );

    @Transactional
    void clear();

    @Transactional
    void removeAll();

    @Nullable Project removeOne(
            @Nullable String userId,
            @Nullable Project project
    );

    @Nullable Project removeOneById(
            @Nullable String id
    );

    @NotNull Project updateById(
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    );

    @NotNull Project changeProjectStatusById(
            @Nullable String id,
            @Nullable Status status
    );

    boolean existsById(@Nullable String userId, @Nullable String id);

    long getSize();
}
