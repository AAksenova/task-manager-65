package ru.t1.aksenova.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import ru.t1.aksenova.tm.entity.dto.TaskDTO;

import java.util.List;

public interface ITaskEndpoint {
    @NotNull
    @PostMapping("/add")
    TaskDTO add(@RequestBody @NotNull TaskDTO task);

    @GetMapping("/findAll")
    List<TaskDTO> findAll();

    @NotNull
    @PostMapping("/save")
    void save(@RequestBody @NotNull TaskDTO task);

    @Nullable
    @GetMapping("/findById/{id}")
    TaskDTO findById(@PathVariable("id") @NotNull String id);

    @GetMapping("/existsById/{id}")
    boolean existsById(@PathVariable("id") @NotNull String id);

    @GetMapping("/count")
    long count();

    @PostMapping("/deleteById/{id}")
    void deleteById(@PathVariable("id") @NotNull String id);

    @PostMapping("/delete")
    void delete(@RequestBody @NotNull TaskDTO task);

    @PostMapping("/deleteAll")
    void clear();
}
