package ru.t1.aksenova.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.t1.aksenova.tm.api.service.dto.IProjectDTOService;
import ru.t1.aksenova.tm.api.service.dto.ITaskDTOService;
import ru.t1.aksenova.tm.entity.dto.ProjectDTO;
import ru.t1.aksenova.tm.enumerated.Status;

@Controller
public class ProjectController {

    @Autowired
    private IProjectDTOService projectService;

    @Autowired
    private ITaskDTOService taskService;

    @GetMapping("project/create")
    public String create() {
        projectService.add(new ProjectDTO("New Project " + System.currentTimeMillis(), Status.NOT_STARTED));
        return "redirect:/projects";
    }

    @GetMapping("project/delete/{id}")
    public String delete(@PathVariable("id") String id) {
        taskService.removeByProjectId(id);
        projectService.removeOneById(id);
        return "redirect:/projects";
    }

    @PostMapping("project/edit/{id}")
    public String edit(@ModelAttribute("project") ProjectDTO project, BindingResult result) {
        projectService.update(project);
        return "redirect:/projects";
    }

    @GetMapping("project/edit/{id}")
    public ModelAndView edit(@PathVariable("id") String id) {
        final ProjectDTO project = projectService.findOneById(id);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("project-edit");
        modelAndView.addObject("project", project);
        modelAndView.addObject("statuses", getStatuses());
        return modelAndView;
    }

    public Status[] getStatuses() {
        return Status.values();
    }

}
