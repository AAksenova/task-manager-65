package ru.t1.aksenova.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.aksenova.tm.api.service.dto.ITaskDTOService;
import ru.t1.aksenova.tm.entity.dto.TaskDTO;
import ru.t1.aksenova.tm.enumerated.Status;
import ru.t1.aksenova.tm.exception.entity.TaskNotFoundException;
import ru.t1.aksenova.tm.exception.field.*;
import ru.t1.aksenova.tm.repository.dto.TaskDTORepository;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
public class TaskDTOService implements ITaskDTOService {

    @Nullable
    @Autowired
    private TaskDTORepository repository;

    @NotNull
    @Override
    public TaskDTORepository getRepository() {
        return repository;
    }

    @Override
    @NotNull
    @Transactional
    public TaskDTO add(@NotNull final TaskDTO task) {
        getRepository().save(task);
        return task;
    }

    @Override
    @Transactional
    public void update(@NotNull TaskDTO task) {
        getRepository().save(task);
    }

    @Override
    @Transactional
    public void remove(@NotNull TaskDTO task) {
        getRepository().delete(task);
    }

    @NotNull
    @Override
    public TaskDTO create(@Nullable final TaskDTO task) {
        if (task == null) throw new TaskNotFoundException();
        getRepository().save(task);
        return task;
    }

    @NotNull
    @Override
    public TaskDTO create(
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull TaskDTO task = new TaskDTO();
        task.setName(name);
        task.setDescription(description);
        getRepository().save(task);
        return task;
    }

    @NotNull
    @Override
    public Collection<TaskDTO> set(@NotNull Collection<TaskDTO> tasks) {
        if (tasks.isEmpty()) return Collections.emptyList();
        tasks.forEach(getRepository()::save);
        return tasks;
    }

    @NotNull
    @Override
    public List<TaskDTO> findAll() {
        return getRepository().findAll();
    }

    @Nullable
    @Override
    public TaskDTO findOneById(
            @Nullable final String id
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final Optional<TaskDTO> task = getRepository().findById(id);
        if (!task.isPresent()) throw new TaskNotFoundException();
        return task.orElse(null);
    }

    @NotNull
    @Override
    public void removeByProjectId(
            @Nullable final String projectId
    ) {
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        getRepository().deleteByProjectId(projectId);
    }

    @Override
    @Transactional
    public void clear() {
        getRepository().deleteAll();
    }

    @Override
    @Transactional
    public void removeAll() {
        getRepository().deleteAll();
    }

    @Nullable
    @Override
    public TaskDTO removeOne(
            @Nullable final TaskDTO task
    ) {
        if (task == null) throw new TaskNotFoundException();
        remove(task);
        return task;
    }

    @Nullable
    @Override
    public TaskDTO removeOneById(
            @Nullable final String id
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final TaskDTO task = findOneById(id);
        remove(task);
        return task;
    }

    @NotNull
    @Override
    public TaskDTO updateById(
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @Nullable final TaskDTO task = findOneById(id);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        update(task);
        return task;
    }

    @NotNull
    @Override
    public TaskDTO changeTaskStatusById(
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null) throw new StatusIncorrectException();
        @Nullable final TaskDTO task = findOneById(id);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        update(task);
        return task;
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return (getRepository().findById(id).orElse(null) != null);
    }

    @Override
    public long getSize() {
        return getRepository().count();
    }

}
