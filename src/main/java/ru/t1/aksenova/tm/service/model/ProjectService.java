package ru.t1.aksenova.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.aksenova.tm.api.service.model.IProjectService;
import ru.t1.aksenova.tm.entity.model.Project;
import ru.t1.aksenova.tm.enumerated.Status;
import ru.t1.aksenova.tm.exception.entity.ProjectNotFoundException;
import ru.t1.aksenova.tm.exception.field.*;
import ru.t1.aksenova.tm.repository.model.ProjectRepository;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
public class ProjectService implements IProjectService {

    @Nullable
    @Autowired
    private ProjectRepository repository;

    @NotNull
    @Override
    public ProjectRepository getRepository() {
        return repository;
    }

    @NotNull
    @Override
    @Transactional
    public Project add(@NotNull final Project project) {
        getRepository().save(project);
        return project;
    }

    @Override
    @Transactional
    public void update(@NotNull Project project) {
        getRepository().save(project);
    }

    @Override
    @Transactional
    public void remove(@NotNull Project project) {
        getRepository().delete(project);
    }

    @NotNull
    @Override
    public Project create(@Nullable final Project project) {
        if (project == null) throw new ProjectNotFoundException();
        add(project);
        return project;
    }

    @NotNull
    @Override
    public Project create(
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        add(project);
        return project;
    }

    @NotNull
    @Override
    @Transactional
    public Collection<Project> set(@NotNull Collection<Project> projects) {
        if (projects.isEmpty()) return Collections.emptyList();
        projects.forEach(getRepository()::save);
        return projects;
    }

    @NotNull
    @Override
    public List<Project> findAll() {
        return getRepository().findAll();
    }

    @Nullable
    @Override
    public Project findOneById(
            @Nullable final String id
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final Optional<Project> project = getRepository().findById(id);
        if (!project.isPresent()) throw new ProjectNotFoundException();
        return project.orElse(null);
    }

    @Override
    @Transactional
    public void clear() {
        getRepository().deleteAll();
    }

    @Override
    @Transactional
    public void removeAll() {
        getRepository().deleteAll();
    }

    @Nullable
    @Override
    public Project removeOne(
            @Nullable final String userId,
            @Nullable final Project project
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (project == null) throw new ProjectNotFoundException();
        remove(project);
        return project;
    }

    @Nullable
    @Override
    public Project removeOneById(
            @Nullable final String id
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final Project project = findOneById(id);
        remove(project);
        return project;
    }

    @NotNull
    @Override
    public Project updateById(
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @Nullable final Project project = findOneById(id);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        update(project);
        return project;
    }

    @NotNull
    @Override
    public Project changeProjectStatusById(
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null) throw new StatusIncorrectException();
        @Nullable final Project project = findOneById(id);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        update(project);
        return project;
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) throw new ProjectIdEmptyException();
        return getRepository().findById(id).orElse(null) != null;
    }

    @Override
    public long getSize() {
        return getRepository().count();
    }

}
