package ru.t1.aksenova.tm.exception.user;

public final class ExistEmailException extends AbstractUserException {

    public ExistEmailException() {
        super("Error! This email already exists...");
    }

    public ExistEmailException(String message) {
        super("Error! This email \"" + message + "\" already exists...");
    }

}
